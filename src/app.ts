import {
    Application,
    Graphics,
    loader,
    Sprite,
    Text,
    TextStyle,
} from 'pixi.js';

import { keyboard } from './utils/keyboard';
import { hitTestRectangle } from './utils/hitTestRectangle';

//Aliases
const { resources } = loader;

//Create a Pixi Application
let app = new Application({
    width: 512,
    height: 512,
    antialias: true,
    transparent: false,
    resolution: 1,
});

//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);

//load an image and run the `setup` function when it's done
loader.add('images/cat.png').load(setup);

//Define any variables that are used in more than one function
let cat: Sprite & {
    vx?: number;
    vy?: number;
};
let box: Graphics;
let message: Text;
let state: (delta: number) => void;

function setup() {
    //Create the box
    box = new Graphics();
    box.beginFill(0xccff99);
    box.drawRect(0, 0, 64, 64);
    box.endFill();
    box.position.set(120, 96);
    app.stage.addChild(box);

    //Create the `cat` sprite
    cat = new Sprite(resources['images/cat.png'].texture);
    cat.y = 96;
    cat.vx = 0;
    cat.vy = 0;
    app.stage.addChild(cat);

    //Capture the keyboard arrow keys
    const left = keyboard('ArrowLeft');
    const up = keyboard('ArrowUp');
    const right = keyboard('ArrowRight');
    const down = keyboard('ArrowDown');

    //Left arrow `press` method
    left.press = () => {
        //Change the cat's velocity whet the key is pressed
        cat.vx = -5;
        cat.vy = 0;
    };

    //Left arrow key `release` method
    left.release = () => {
        //If the left arrow has been released, and the right arrow isn't down,
        //and the cat isn't moving vertically:
        //Stop the cat
        if (!right.isDown && cat.vy === 0) {
            cat.vx = 0;
        }
    };

    //Up
    up.press = () => {
        cat.vy = -5;
        cat.vx = 0;
    };
    up.release = () => {
        if (!down.isDown && cat.vx === 0) {
            cat.vy = 0;
        }
    };

    //Right
    right.press = () => {
        cat.vx = 5;
        cat.vy = 0;
    };
    right.release = () => {
        if (!left.isDown && cat.vy === 0) {
            cat.vx = 0;
        }
    };

    //Down
    down.press = () => {
        cat.vy = 5;
        cat.vx = 0;
    };
    down.release = () => {
        if (!up.isDown && cat.vx === 0) {
            cat.vy = 0;
        }
    };

    //Create the text sprite
    const style = new TextStyle({
        fontFamily: 'sans-serif',
        fontSize: 18,
        fill: 'white',
    });
    message = new Text('No collision...', style);
    message.position.set(8, 8);
    app.stage.addChild(message);

    //Set the play state
    state = play;

    //Start the game loop
    app.ticker.add(delta => gameLoop(delta));
}

function gameLoop(delta: number) {
    //Update the current game state:
    state(delta);
}

//@ts-ignore
function play(delta: number) {
    //Use the cat's velocity to make it move
    cat.x += cat.vx;
    cat.y += cat.vy;

    //Check for a collision between the cat and the box
    if (hitTestRectangle(cat, box)) {
        //if there's a collision, change the message text
        //and tint the box red
        message.text = 'hit!';
        box.tint = 0xff3300;
    } else {
        //if there's no collision, reset the message
        //text and the box's color
        message.text = 'No collision...';
        box.tint = 0xccff99;
    }
}
