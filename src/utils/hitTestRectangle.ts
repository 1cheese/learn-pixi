import { Container } from 'pixi.js';

export function hitTestRectangle(
    rectangle1: Container,
    rectangle2: Container,
): boolean {
    //Define the variables we'll need to calculate
    let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

    //hit will determine whether there's a collision
    hit = false;

    //Find the center points of each sprite
    const rectangle1CenterX = rectangle1.x + rectangle1.width / 2;
    const rectangle1CenterY = rectangle1.y + rectangle1.height / 2;

    const rectangle2CenterX = rectangle2.x + rectangle2.width / 2;
    const rectangle2CenterY = rectangle2.y + rectangle2.height / 2;

    //Find the half-widths and half-heights of each sprite
    const rectangle1HalfWidth = rectangle1.width / 2;
    const rectangle1HalfHeight = rectangle1.height / 2;

    const rectangle2HalfWidth = rectangle2.width / 2;
    const rectangle2HalfHeight = rectangle2.height / 2;

    //Calculate the distance vector between the sprites
    vx = rectangle1CenterX - rectangle2CenterX;
    vy = rectangle1CenterY - rectangle2CenterY;

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = rectangle1HalfWidth + rectangle2HalfWidth;
    combinedHalfHeights = rectangle1HalfHeight + rectangle2HalfHeight;

    //Check for a collision on the x axis
    if (Math.abs(vx) < combinedHalfWidths) {
        //A collision might be occurring. Check for a collision on the y axis
        if (Math.abs(vy) < combinedHalfHeights) {
            //There's definitely a collision happening
            hit = true;
        } else {
            //There's no collision on the y axis
            hit = false;
        }
    } else {
        //There's no collision on the x axis
        hit = false;
    }

    //`hit` will be either `true` or `false`
    return hit;
}
