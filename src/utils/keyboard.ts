export type KeyType = {
    value: KeyboardEvent['key'];
    isDown: boolean;
    isUp: boolean;
    press?: () => void;
    release?: () => void;
    downHandler: (event: KeyboardEvent) => void;
    upHandler: (event: KeyboardEvent) => void;
};

export function keyboard(value: KeyboardEvent['key']) {
    const key: KeyType = {
        value: value,
        isDown: false,
        isUp: true,
        press: undefined,
        release: undefined,
        downHandler: function (event) {
            if (event.key === this.value) {
                if (this.isUp && this.press) {
                    this.press();
                }
                this.isDown = true;
                this.isUp = false;
                event.preventDefault();
            }
        },
        upHandler: function (event) {
            if (event.key === this.value) {
                if (this.isDown && this.release) {
                    this.release();
                }
                this.isDown = false;
                this.isUp = true;
                event.preventDefault();
            }
        },
    };

    //Attach event listeners
    const downListener = key.downHandler.bind(key);
    const upListener = key.upHandler.bind(key);

    window.addEventListener('keydown', downListener, false);
    window.addEventListener('keyup', upListener, false);

    return key;
}
